const express = require('express')
const { version } = require('../package.json');

module.exports = class Express {
  constructor(port, websocket_port) {
    this.port = port
    this.websocket_port = websocket_port
    this.start()
  }
  // TODO: Start the service
  start () {
    // create obj
    const app = express()
    // get html
    let html = this.getHtml()
    // set url path
    app.get('/', (req, res) => {
      res.send(html)
    })
    // start server
    this.server = app.listen(this.port, () => {
      console.log('');
      console.log(`Example app listening on port ${this.port}`)
      console.log('');
      console.log(`Please open: http://127.0.0.1:${this.port}/`);
      console.log('');
    })
    // Determine if the port is occupied
    this.server.on('error', (error) => {
      if (error.code !== 'EADDRINUSE') { // System non-listening port operation error
        throw error
      }
    })
  }
  // TODO: Shut down service
  close () {
    this.server && this.server.close()
  }
  // TODO: Get return html
  getHtml () {
    return `
    <!DOCTYPE html>
    <html lang="en">

    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>LBROW</title>
    </head>

    <body>
      <script>
        let socket = null
        let package = 'log-browser'
        let time = new Date().toLocaleDateString()
        let version = 'v${version}'
        let css0 = 'color:#fff;background-color:#0099ff;line-height:20px;border-radius: 5px 0 0 5px'
        let css1 = 'color:#fff;background-color:#494949;line-height:20px'
        let css2 = 'color:#fff;background-color:orange;line-height:20px;border-radius: 0 5px 5px 0'
        let log = \`%c \${package} %c \${time} %c \${version} \`
        console.log(log, css0, css1, css2);
        function init () {
          if ('WebSocket' in window) {
              // Create WebSocket connection.
              socket = new WebSocket('ws://localhost:${this.websocket_port}');
              // Listen for messages
              socket.addEventListener('message', function (event) {
                  console.clear()
                  console.log(log, css0, css1, css2);
                  let res = JSON.parse(event.data)
                  // array
                  if (Array.isArray(res)) {
                      for (const i in res) {
                          let v = res[i]
                          console.log(v);
                      }
                  }
                  else {
                      console.log(res);
                  }
              })
              socket.addEventListener('close', function (event) {
                  socket && socket.close();
                  init()
                  console.warn('link closed')
              });
              socket.addEventListener('error', function (event) {
                  socket && socket.close();
              });
          }
      }
      init()
    </script>
    </body>

    </html>
    `
  }
}