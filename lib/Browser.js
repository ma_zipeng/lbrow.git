let Express = require('./Express')
let WebSocket = require('./WebSocket')

module.exports = class Browser {
    constructor(express_port = 3000, websocket_port = 8001) {
        this.EXPRESS_INSTANCE = new Express(express_port, websocket_port)
        this.WEBSOCKET_INSTANCE = new WebSocket(websocket_port)
    }
    // TODO: 输出数据
    async log (msg) {
        if (typeof msg === 'function') {
            msg = msg.toString()
        }
        this.WEBSOCKET_INSTANCE.sendContent.push(msg)
    }
}