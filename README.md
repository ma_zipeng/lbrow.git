# lbrow

#### 介绍

可以将 nodejs 的输出内容，打印到浏览器上，方便查看

#### 使用说明

tip: 如果使用时，发现端口被占用，传入对应参数进行更替

```js
const Browser = require("lbrow");

let browser = new Browser(); // Browser(express_port = 3000, websocket_port = 8001)
browser.log(1);
browser.log("b");
browser.log("c");
browser.log("牛逼啊老铁");
browser.log({ a: 1, b: 2 });
browser.log(process.pid);
browser.log(process.ppid);
```
